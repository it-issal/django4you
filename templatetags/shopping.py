#-*- coding: utf-8 -*-

from datetime import time, date, datetime, timedelta
import dateutil.parser

import simplejson as json
import requests

################################################################################

from django                    import template
from django.utils              import html
from django.utils              import timezone as TZ
from django.utils.numberformat import format   as number_format

register = template.Library()

################################################################################

CURRENCY_MAPPER = {
    u"£":    'GBP',
    u"$":    'USD',
}

@register.filter(is_safe=True)
def money(cash, devise='EUR'):
    if type(cash.get('value', None)) in (str, unicode):
        for k,v in [(u'\xa0',''),(',','.'),(' ','')]:
            while k in cash['value']:
                cash['value'] = cash['value'].replace(k,v)

    payload = {
        'from': CURRENCY_MAPPER.get(unicode(cash['devise']), cash['devise']),
        'to':   CURRENCY_MAPPER.get(unicode(devise), devise),
        'q':    float(cash['value']),
    }

    if payload['from']!=payload['to']:
        resp = requests.get('http://rate-exchange.appspot.com/currency', params=payload)

        payload['q'] = number_format(resp.json()['v'], ".", 2)

    return "%(q)s %(to)s" % payload

