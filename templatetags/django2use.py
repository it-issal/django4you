#-*- coding: utf-8 -*-

from datetime import time, date, datetime, timedelta
import dateutil.parser

import simplejson as json
import requests
import sys

################################################################################

from django                      import template
from django.conf                 import settings
from django.utils                import html
from django.utils                import timezone as TZ
from django.utils.encoding       import force_text, smart_text
from django.utils.numberformat   import format   as number_format
from subdomains.utils            import reverse as subdomain_reverse

register = template.Library()

################################################################################

@register.filter(is_safe=True)
def timestamp(value):
    return time.asctime(time.gmtime(value))

################################################################################

@register.filter(is_safe=True)
def floatdot(value, decimal_pos=2):
    return number_format(value or 0, ".", decimal_pos)

#************************************************************************************

def to_date(value):
    if type(value) not in (time, date, datetime):
        try:
            value = datetime.fromtimestamp(long(value))
        except Exception,ex:
            print ex

            value = dateutil.parser.parse(value)

    return value

@register.filter(is_safe=True)
def since(value):
    return humanize_date_difference(to_date(value), datetime.now())

def humanize_date_difference(now, otherdate=None, offset=None):
    if otherdate:
        dt = otherdate - now
        offset = dt.seconds + (dt.days * 60*60*24)
    if offset:
        delta_s = offset % 60
        offset /= 60
        delta_m = offset % 60
        offset /= 60
        delta_h = offset % 24
        offset /= 24
        delta_d = offset
    else:
        raise ValueError("Must supply otherdate or offset (from now)")

    if delta_d > 1:
        if delta_d > 6:
            date = now + timedelta(days=-delta_d, hours=-delta_h, minutes=-delta_m)
            return date.strftime('%A, %Y %B %m, %H:%I')
        else:
            wday = now + timedelta(days=-delta_d)
            return wday.strftime('%A')
    if delta_d == 1:
        return "Yesterday"
    if delta_h > 0:
        return "%dh%dm ago" % (delta_h, delta_m)
    if delta_m > 0:
        return "%dm%ds ago" % (delta_m, delta_s)
    else:
        return "%ds ago" % delta_s

#************************************************************************************

@register.filter(is_safe=True)
def ucfirst(value):
    resp = unicode(value).capitalize()

    return resp

#####################################################################################

@register.filter(is_safe=True)
def field_css(value, arg):
    return value.as_widget(attrs={'class': arg})

#####################################################################################

@register.filter(name='json', is_safe=True)
def to_json(value, indent=4):
    return json.dumps(value, sort_keys=True, indent=indent, separators=(',%s\n' % (' ' * indent), ':'))

#************************************************************************************

@register.filter(is_safe=True)
def pretty_json(value):
    resp = json.dumps(value, sort_keys=True, indent=4, separators=(',\n', ':'))

    return html.escape(resp)

################################################################################
################################################################################
################################################################################

from django.template.base import (
    BLOCK_TAG_END, BLOCK_TAG_START, COMMENT_TAG_END, COMMENT_TAG_START,
    SINGLE_BRACE_END, SINGLE_BRACE_START, VARIABLE_ATTRIBUTE_SEPARATOR,
    VARIABLE_TAG_END, VARIABLE_TAG_START, Context, InvalidTemplateLibrary,
    Library, Node, NodeList, Template, TemplateSyntaxError,
    VariableDoesNotExist, get_library, kwarg_re, render_value_in_context,
    token_kwargs,
)

class URLNode(Node):
    def __init__(self, view_name, args, kwargs, asvar):
        self.view_name = view_name
        self.args = args
        self.kwargs = kwargs
        self.asvar = asvar

    def render(self, context):
        from django.core.urlresolvers import reverse, NoReverseMatch
        args = [arg.resolve(context) for arg in self.args]
        kwargs = dict((smart_text(k, 'ascii'), v.resolve(context))
                      for k, v in self.kwargs.items())

        view_name = self.view_name.resolve(context)

        try:
            current_app = context.request.current_app
        except AttributeError:
            # Change the fallback value to None when the deprecation path for
            # Context.current_app completes in Django 2.0.
            current_app = context.current_app

        # Try to look up the URL twice: once given the view name, and again
        # relative to what we guess is the "main" app. If they both fail,
        # re-raise the NoReverseMatch unless we're using the
        # {% url ... as var %} construct in which case return nothing.
        extra = {}

        for x in ('subdomain','scheme','current_app'):
            if x in kwargs:
                extra[x] = kwargs[x]

                del kwargs[x]

        if len(args):   extra['args']   = args
        if len(kwargs): extra['kwargs'] = kwargs

        if extra.get('subdomain', None) is None:
            if 'request' in context:
                extra['subdomain'] = context['request'].subdomain
            elif current_app=='admin':
                extra['subdomain'] = 'www'
        
        url = ''
        
        try:
            url = subdomain_reverse(view_name, **extra)
        except NoReverseMatch:
            exc_info = sys.exc_info()
            if settings.SETTINGS_MODULE:
                project_name = settings.SETTINGS_MODULE.split('.')[0]
                try:
                    url = subdomain_reverse(project_name + '.' + view_name, **extra)
                except NoReverseMatch:
                    if self.asvar is None:
                        # Re-raise the original exception, not the one with
                        # the path relative to the project. This makes a
                        # better error message.
                        
                        six.reraise(*exc_info)
                        
                        return ''
            else:
                if self.asvar is None:
                    raise

        if self.asvar:
            context[self.asvar] = url
            return ''
        else:
            return url

################################################################################

@register.tag
def url(parser, token):
    bits = token.split_contents()
    if len(bits) < 2:
        raise TemplateSyntaxError("'%s' takes at least one argument"
                                  " (path to a view)" % bits[0])
    viewname = parser.compile_filter(bits[1])
    args = []
    kwargs = {}
    asvar = None
    bits = bits[2:]
    if len(bits) >= 2 and bits[-2] == 'as':
        asvar = bits[-1]
        bits = bits[:-2]

    if len(bits):
        for bit in bits:
            match = kwarg_re.match(bit)
            if not match:
                raise TemplateSyntaxError("Malformed arguments to url tag")
            name, value = match.groups()
            if name:
                kwargs[name] = parser.compile_filter(value)
            else:
                args.append(parser.compile_filter(value))
    
    return URLNode(viewname, args, kwargs, asvar)

