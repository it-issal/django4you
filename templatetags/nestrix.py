#-*- coding: utf-8 -*-

from datetime import time, date, datetime, timedelta
import dateutil.parser

import simplejson as json
import requests

################################################################################

from django                    import template
from django.utils              import html
from django.utils              import timezone as TZ
from django.utils.numberformat import format   as number_format

register = template.Library()

################################################################################

@register.simple_tag(takes_context=True)
def cdn(context, *path):
    return '/'.join(['//cdn.maher.media'] + list(path))

@register.simple_tag(takes_context=True)
def media(context, *path):
    return '/'.join([context['CDN_PREFIX'], 'media'] + list(path))
    #return '/'.join([context['CDN_PREFIX'], 'media'] + list(path))

@register.simple_tag(takes_context=True)
def static(context, *path):
    #return '/'.join([context['CDN_PREFIX'], 'static'] + list(path))
    return '/'.join(['/static'] + list(path))

################################################################################

@register.simple_tag(takes_context=True)
def trans(context, litteral, lang=None):
    return litteral

################################################################################

def jeditable(context, narrow, default=None, lang='en', scope='page'):
    slug = narrow
    
    for key in ('_','.'):
        while key in slug:
            slug = slug.replace(key,'-')
    
    try:
        context['profile'] = context['request'].user.profile
    except Exception,ex:
        context['profile'] = None
        
        #raise ex
        
        context.update({
            'value':   default,
            'as_text': unicode(default),
        })
        
        return context
    
    if scope=='page':
        context['page'].save()
        
        ts, st = context['page'].trans.get_or_create(
            page     = context['page'],
            language = lang,
            narrow   = narrow,
        )
        
        value = ts.reflection or default
        
        if st:
            if ts.reflection!=None:
                ts.reflection = value
            
            ts.save()
        
        context['entry']  = ts
        context['uri_qs'] = 'page={{ entry.page.pk }}'
    elif scope=='site':
        ts, st = context['site_config'].get_or_create(
            language = lang,
            narrow   = narrow,
        )
        
        value = ts.reflection or default
        
        if st:
            if ts.reflection!=None:
                ts.reflection = value
            
            ts.save()
        
        context['entry']  = ts
        context['uri_qs'] = ''
    
    if len(context['uri_qs'])==0:
        context['uri_qs'] = 'narrow=%s' % context['entry'].narrow
    else:
        context['uri_qs'] = 'narrow=%s&%s' % (context['entry'].narrow, context['uri_qs'])
    
    context.update({
        'tag_id':  'page-dynamic-%s' % slug,
        'value':   value,
        'as_text': unicode(value),
    })
    
    return context

@register.inclusion_tag('gabarits/extras/jeditable/translate.html', takes_context=True)
def editable(context, narrow, *args, **kwargs):
    kwargs['scope'] = 'page'
    
    return jeditable(context, narrow, *args, **kwargs)

@register.inclusion_tag('gabarits/extras/jeditable/setting.html', takes_context=True)
def configurable(context, narrow, *args, **kwargs):
    kwargs['scope'] = 'site'
    
    return jeditable(context, narrow, *args, **kwargs)

################################################################################

@register.inclusion_tag('gabarits/extras/jeditable/model_field.html', takes_context=True)
def model_field(context, instance, field, default=None, lang='en'):
    context['vector'] = dict(
        schema = '.'.join([instance._meta.app_label, instance._meta.model_name]),
        narrow = instance.pk,
        field  = field,
    )
    
    context['value'] = getattr(instance, field, default)
    context['as_text'] = unicode(context['value'])
    
    return context
    
    slug = narrow
    
    for key in ('_','.'):
        while key in slug:
            slug = slug.replace(key,'-')
    
    try:
        context['profile'] = context['request'].user.profile
    except Exception,ex:
        context['profile'] = None
        
        #raise ex
        
        context.update({
            'value':   default,
            'as_text': unicode(default),
        })
        
        return context
    
    if scope=='page':
        context['page'].save()
        
        ts, st = context['page'].trans.get_or_create(
            page     = context['page'],
            language = lang,
            narrow   = narrow,
        )
        
        value = ts.reflection or default
        
        if st:
            if ts.reflection!=None:
                ts.reflection = value
            
            ts.save()
        
        context['entry']  = ts
        context['uri_qs'] = 'page={{ entry.page.pk }}'
    elif scope=='site':
        ts, st = context['site_config'].get_or_create(
            language = lang,
            narrow   = narrow,
        )
        
        value = ts.reflection or default
        
        if st:
            if ts.reflection!=None:
                ts.reflection = value
            
            ts.save()
        
        context['entry']  = ts
        context['uri_qs'] = ''
    
    if len(context['uri_qs'])==0:
        context['uri_qs'] = 'narrow=%s' % context['entry'].narrow
    else:
        context['uri_qs'] = 'narrow=%s&%s' % (context['entry'].narrow, context['uri_qs'])
    
    context.update({
        'tag_id':  'page-dynamic-%s' % slug,
        'value':   value,
        'as_text': unicode(value),
    })
    
    return context

################################################################################

@register.inclusion_tag('gabarits/core/form.html', takes_context=True)
def render_form(context, *args, **kwargs):
    kwargs['scope'] = 'model'
    
    return jeditable(context, *args, **kwargs)

################################################################################

@register.inclusion_tag('gabarits/core/carousel.html', takes_context=True)
def carousel(context, slides=None, active=0, **kwargs):
    if slides is None:
        slides = [
            slide
            for slide in context['page'].sliders.all()
        ]
    
    context.update({
        'listing':       slides,
        'active_slide': active,
    })
    
    return context

