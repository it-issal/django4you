# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('django2use', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='WebSetting',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('language', models.CharField(default=None, max_length=64, null=True, blank=True)),
                ('narrow', models.CharField(max_length=512)),
                ('litteral', models.TextField(blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
    ]
