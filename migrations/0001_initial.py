# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('user', models.OneToOneField(related_name='profile', to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Profile_SSH',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('username', models.CharField(max_length=64)),
                ('pub_key', models.TextField(blank=True)),
                ('priv_key', models.TextField(blank=True)),
                ('profile', models.ForeignKey(related_name='ssh_keys', to='django2use.Profile')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Role',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64)),
                ('title', models.CharField(max_length=256, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='WebAuthor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('fullname', models.CharField(max_length=256, blank=True)),
                ('email', models.EmailField(max_length=75)),
                ('owner', models.ForeignKey(related_name='authoring', to='django2use.Profile')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='WebPage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('subdomain', models.CharField(max_length=32)),
                ('path', models.CharField(max_length=512, blank=True)),
                ('title', models.CharField(max_length=256, blank=True)),
                ('description', models.CharField(max_length=256, blank=True)),
                ('keywords', models.TextField(blank=True)),
                ('content', models.TextField(blank=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('published_on', models.DateTimeField(default=None, null=True, blank=True)),
                ('last_updated', models.DateTimeField(auto_now_add=True)),
                ('author', models.ForeignKey(related_name='publications', default=None, blank=True, to='django2use.WebAuthor', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='WebTranslation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('language', models.CharField(default=None, max_length=64, null=True, blank=True)),
                ('narrow', models.CharField(max_length=512)),
                ('litteral', models.TextField(blank=True)),
                ('page', models.ForeignKey(related_name='trans', to='django2use.WebPage')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='WebWidget',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('provider', models.CharField(max_length=64)),
                ('config', models.TextField(default=b'{}')),
                ('page', models.ForeignKey(related_name='widgets', to='django2use.WebPage')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
