# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('django2use', '0003_webslider_websliderentry'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='webpage',
            options={'ordering': ['subdomain', 'path']},
        ),
        migrations.AlterModelOptions(
            name='webslider',
            options={'ordering': ['page', 'offset']},
        ),
        migrations.AlterModelOptions(
            name='websliderentry',
            options={'ordering': ['slider', 'offset']},
        ),
        migrations.AlterModelOptions(
            name='webtranslation',
            options={'ordering': ['page', 'language', 'narrow']},
        ),
        migrations.AddField(
            model_name='webpage',
            name='active_slide',
            field=models.PositiveIntegerField(default=0),
            preserve_default=True,
        ),
    ]
