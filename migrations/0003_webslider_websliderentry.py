# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('django2use', '0002_websetting'),
    ]

    operations = [
        migrations.CreateModel(
            name='WebSlider',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('layout', models.CharField(default=b'ltr', max_length=16, choices=[(b'ltr', b'Image left, Text right'), (b'rtl', b'Image right, Text left'), (b'circles', b'Circles with texts')])),
                ('offset', models.PositiveIntegerField(default=0)),
                ('title', models.CharField(max_length=256, blank=True)),
                ('content', models.TextField(blank=True)),
                ('cover', models.CharField(max_length=256, blank=True)),
                ('page', models.ForeignKey(related_name='sliders', to='django2use.WebPage')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='WebSliderEntry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('offset', models.PositiveIntegerField(default=0)),
                ('css_class', models.CharField(max_length=256, blank=True)),
                ('content', models.TextField(blank=True)),
                ('slider', models.ForeignKey(related_name='entries', to='django2use.WebSlider')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
