# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('django2use', '0004_auto_20150301_2230'),
    ]

    operations = [
        migrations.AddField(
            model_name='webslider',
            name='css_class',
            field=models.CharField(default='', max_length=256, blank=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='websliderentry',
            name='css_icon',
            field=models.CharField(default='', max_length=64, blank=True),
            preserve_default=False,
        ),
    ]
