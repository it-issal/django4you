#-*- coding: utf-8 -*-

from django2use.helpers import *

################################################################################

class MongoORM(object):
    def __init__(self):
        self._setup = False
        self._cfg   = {}
        self._reg   = {}

    def ensure(self, stt):
        if self._setup==False:
            for alias in stt:
                if True: #alias not in ['default']:
                    cfg = urlparse(stt[alias])

                    self._reg[alias] = mongo.connect(cfg.path[1:], alias,
                        host=cfg.hostname,
                        port=cfg.port,
                        username=cfg.username,
                        password=cfg.password,
                    )

                    self._cfg[alias] = cfg

        self._setup = True

    ############################################################################

    def aggregate(self, coll, pivot, op='$sum', target='value'):
        query = coll.aggregate([
            {
                "$group": {
                    '_id': pivot,
                    target: {
                        op: 1
                    },
                },
            },{
                "$sort": SON([
                    (target, -1),
                    ('_id',  -1),
                ])
            }
        ])

        return query['result']

    ############################################################################

    def access_coll(self, conn, db, creds):
        sch = 'test_sch'

        try:
            if sch not in conn[db].collection_names():
                conn[db].create_collection(sch)

            conn[db].drop_collection(sch)

            return conn[db][sch]
        except pymongo.errors.OperationFailure,ex:
            if ('username' in creds) and ('password' in creds):
                if conn[db].authenticate(creds['username'], creds['password']):
                    pass # log('mongodb', 'authnticated to %(database)s at %(host)s ')
                else:
                    pass # throw('mongodb', 'Wrong credentials')

        return None

    #---------------------------------------------------------------------------

    @property
    def old_stats(self):
        resp = {}

        for db in self._reg:
            crs = self._reg[db]

            resp[db] = {}

            stats = crs[db].command("dbstats")

            for key in stats:
                resp[db][key] = crs[db].command("collstats", key)

        return resp

    def coll_stats(self, db, sch):
        try:
            db.validate_collection(sch)
        except:
            return None

        return db.command({
            'collStats': sch,
        })

    FORBIDDEN = [
        'objectlabs-system',
        'objectlabs-system.admin.collections',
    ]

    MAPPING = {
        'native':  ('default', 'scubadev'),
        'octopus': ('default', 'octopus'),
        'vortex':  ('default', 'vortex'),
    }

    STATIC_COLLs = [
        'job_offer',
    ]+[
        prefix + x
        for prefix,lst in [
            ('cloud_server_', ['phy','virt']),
            ('retail_',       ['product','article','category']),
        ]
        for x in lst
    ]

    def metrics(self, alias):
        self.ensure()

        if alias in self.MAPPING:
            conn,db = self.MAPPING[alias]

            crs = mongo.connection.get_connection(conn)[db]

            stats  = dict([
                (sch, self.coll_stats(crs, sch))
                for sch in crs.collection_names()
            ])

            resp = dict(
                total     = {'count':0, 'size':0},
                partition = {'count':{}, 'size':{}},
            )

            for key in stats:
                resp['total']['count'] += stats[key]['count']
                resp['total']['size']  += stats[key]['storageSize']

                if (key not in self.FORBIDDEN) and (0 < resp['total']['count']):
                    resp['partition']['count'][key] = stats[key]['count']
                    resp['partition']['size'][key]  = stats[key]['storageSize']

            return {
                'alias':  alias,
                'conn':   conn,
                'db':     db,
                'stats':  stats,
                'quotas': resp,
            }
        else:
            return None

MongoORM = MongoORM()
