#-*- coding: utf-8 -*-

from django2use.helpers import *

from django.utils.numberformat   import format   as number_format

################################################################################

class Money(object):
    def __init__(self, value, devise):
        self._value  = value

        if type(self._value) in (str, unicode):
            self._value = self._value or '0'

            for k,v in [(u'\xa0',''),(',','.'),(' ','')]:
                while k in self._value:
                    self._value = self._value.replace(k,v)

            try:
                self._value = float(self._value)
            except:
                self._value = None

        if not self._value:
            self._value = 0

        self._devise = devise

    value  = property(lambda self: self._value)
    devise = property(lambda self: self._devise)

    def to(self, target):
        if self.devise!=target:
            payload = {
                'from': CURRENCY_MAPPER.get(unicode(self.devise), self.devise),
                'to':   CURRENCY_MAPPER.get(unicode(target), target),
                'q':    float(self.value),
            }

            if payload['from']!=payload['to']:
                try:
                    resp = requests.get('http://rate-exchange.appspot.com/currency', params=payload)
                except requests.ConnectionError,exc:
                    resp = None

                if resp is not None:
                    if resp.return_code==200:
                        return Money(resp.json()['v'], payload['to'])

        return self

    def __str__(self):
        return "%.2f %s" % (
            float(self.value),
            self.devise.upper(),
        )

    def __add__(self, other):
        if self.devise.lower()==other.devise.lower():
            other = other.to(self.devise)

        return Money(self.value + other.value, self.devise)

    def __sub__(self, other):
        if self.devise.lower()==other.devise.lower():
            other = other.to(self.devise)

        return Money(self.value - other.value, self.devise)
