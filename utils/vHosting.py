#-*- coding: utf-8 -*-

from django2use.core.routing.shortcuts import *

#*******************************************************************************

@MultiTenant.Factory.register('mongo')
class MongoTenant(MultiTenant):
    def resolve(self, lookup):
        try:
            return self.schema.objects.get(**lookup)
        except Exception,ex: # DoesNotExist
                return None

#*******************************************************************************

@MultiTenant.Factory.register('orm')
class ORM_Tenant(MultiTenant):
    def resolve(self, lookup):
        return self.schema.get(**lookup)
        
        try:
            pass
        except Exception,ex:
            return None
