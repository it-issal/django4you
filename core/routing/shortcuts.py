from .abstract import vHost
from .mapping  import Urlmap, Sitemap
from .tenancy  import MultiTenant

from .tenancy import RequestTenancy
