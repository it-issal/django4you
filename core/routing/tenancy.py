#-*- coding: utf-8 -*-

from urlparse import urlparse

from django.conf import settings, urls as dj_urls

################################################################################

class RequestTenancy(object):
    def __init__(self, req):
        self._req = req

        self._pvt = {}
        self._dyn = {}

    request   = property(lambda self: self._req)

    vhost     = property(lambda self: self.request.vhost)
    profile   = property(lambda self: self.request.profile)

    pivot     = property(lambda self: self._dyn.values())
    context   = property(lambda self: self._dyn)

    def extend(self, axe, lookup):
        if axe.narrow not in self._pvt:
            self._dyn[axe.narrow] = axe.resolve(lookup)

            self._pvt[axe.narrow] = axe

    def narrow(self, axis):
        if issubclass(type(axis), MultiTenant):
            axis = axis.narrow

        return self._dyn.get(axis, None) or {}

    #***************************************************************************

    def keys(self):     return self.context.keys()
    def values(self):   return self.context.values()

    def __iter__(self): return self.context.__iteritems__()

    def __getitem__(self, key, default=None):
        return self.context.get(key, default)

#*******************************************************************************

class MultiTenant(object):
    class Factory:
        registry = {}

        @classmethod
        def register(cls, *args, **kwargs):
            def do_apply(handler, alias, **opts):
                if alias not in MultiTenant.Factory.registry:
                    cls.registry[alias] = handler

                return handler

            return lambda hnd: do_apply(hnd, *args, **kwargs)

    @classmethod
    def wrap(cls, kind, *args, **kwargs):
        if kind in cls.Factory.registry:
            xtr = cls.Factory.registry[kind]

            if callable(xtr):
                return lambda hnd: xtr(hnd, *args, **kwargs)

        return lambda hnd: hnd

    def __init__(self, callback, fields, model, narrow):
        self._cb   = callback
        self._keys = fields
        self._sch  = model
        self._nrw  = narrow

    callback = property(lambda self: self._cb)
    fields   = property(lambda self: self._keys)
    schema   = property(lambda self: self._sch)
    narrow   = property(lambda self: self._nrw)

    def __call__(self, request, *args, **kwargs):
        if not hasattr(request, 'tenancy'):
            setattr(request, 'tenancy', RequestTenancy(request))

        lookup = {}

        for offset in range(0, len(self.fields)):
            alias,field = self.fields[offset]

            if len(args) < len(self.fields):
                if alias in kwargs:
                    lookup[field] = kwargs[alias]

                    del kwargs[alias]
            else:
                lookup[field] = args[x]

                args = args[1:]

        request.tenancy.extend(self, lookup)

        resp = self.callback(request, *args, **kwargs)

        return resp
