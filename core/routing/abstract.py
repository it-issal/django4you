#-*- coding: utf-8 -*-

from urlparse import urlparse

from django.conf import settings, urls as dj_urls

from .mapping import *

################################################################################

class vHost(Urlmap):
    def initialize(self, callback=None):
        self._cb  = callback

    callback  = property(lambda self: self._cb)
