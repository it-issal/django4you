#-*- coding: utf-8 -*-

from urlparse import urlparse

from django.conf import urls as dj_urls

################################################################################

class RoutingMixin(object):
    def __init__(self, *args, **kwargs):
        self._stc = []
        self._lst = []
        self._sub = []
        self._mws = []

        if hasattr(self, 'initialize'):
            if callable(self.initialize):
                self.initialize(*args, **kwargs)

    ############################################################################

    middlewares = property(lambda self: self._mws)

    ############################################################################

    def require(self, ns, rpath):
        resp = dj_urls.url(ns, rpath)

        self._stc.append(resp)

        return resp

    #***************************************************************************

    def route(self, *args, **kwargs):
        def do_apply(handler, pattern, name=None, csrf=True, policy='anon', wrap=None):
            target = handler

            if csrf==False:
                from django.views.decorators.csrf   import csrf_exempt

                target = csrf_exempt(target)

            if policy=='login':
                from django.contrib.auth.decorators import login_required

                target = login_required(target)

            resp = dj_urls.url(pattern, target, name=(name or handler.__name__))

            self._lst.append(resp)

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    ############################################################################

    def augment(self, *args, **kwargs):
        def do_apply(handler, *args, **kwargs):
            self._mws.append(handler)

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    ############################################################################

    def populate(self):
        for entry in self._stc:
            yield entry

        for entry in self._lst:
            yield self.wrap(entry)

        for partial in self._sub:
            for entry in partial.populate():
                yield self.wrap(entry)

    #***************************************************************************

    def wrap(self, entry):
        for middlet in self.middlewares:
            if callable(middlet):
                resp = entry

                try:
                    resp = middlet(resp)
                except:
                    pass

                if entry is not None:
                    entry = resp

        return entry

    ############################################################################

    def fork(self, prefix, *args, **kwargs):
        resp = Urlmap(self, prefix, *args, **kwargs)

        self._sub.append(resp)

        return resp

################################################################################

class Urlmap(RoutingMixin):
    def initialize(self, parent, prefix, *args, **kwargs):
        self._prnt  = parent
        self._prfx  = prefix

    #***************************************************************************

    parent    = property(lambda self: self._prnt)
    prefix    = property(lambda self: self._prfx)

    namespace = property(lambda self: self.parent.namespace)

################################################################################

registry = {}

#*******************************************************************************

class Sitemap(RoutingMixin):
    @classmethod
    def resolve(cls, alias):
        if alias in registry:
            return registry[alias]
        else:
            return None

    #***************************************************************************

    def initialize(self, alias, *args, **kwargs):
        self._ns  = ''

        if 'ns' in kwargs:
            self._ns  = kwargs['ns']

        self._key = alias

        if self.alias not in registry:
            registry[alias] = self

        self._rh = kwargs.get('rehearse', self.namespace)

        self.ensure()

    #***************************************************************************

    def ensure(self):
        from django.conf import settings

        if self.alias not in settings.SUBDOMAIN_URLCONFS:
            settings.SUBDOMAIN_URLCONFS[self.alias] = self.namespace

    #***************************************************************************

    alias     = property(lambda self: self._key)

    prefix    = property(lambda self: '')
    namespace = property(lambda self: self._ns)
    rehearse  = property(lambda self: self._rh)

    rqdn      = property(lambda self: self._key)
    fqdn      = property(lambda self: '.'.join([self.rqdn, 'scubadev.com']))

    #***************************************************************************

    @property
    def patterns(self):
        return dj_urls.patterns(self.rehearse, *[x for x in self.populate()])
