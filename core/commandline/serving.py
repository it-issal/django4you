#-*- coding: utf-8 -*-

from django2use.shortcuts import *

#*******************************************************************************

from autobahn.asyncio.websocket import WebSocketServerProtocol, WebSocketServerFactory

try:
    import asyncio
except ImportError:
    # Trollius >= 0.3 was renamed
    import trollius as asyncio

################################################################################

class SocketsServer(BaseCommand):
    help = 'Lunch a WebSockets server.'

    ############################################################################

    def enqueue(self, target):
        entry = SocketsPeer(target)

        self.peers.append(entry)

    def handle(self, *args, **options):
        self.peers = []

        self.factory = WebSocketServerFactory(self.Endpoint.realm, debug=False) #settings.DEBUG)
        self.factory.protocol = self.Protocol
        self.factory.command  = self

        print "#) Running the WebSockets server at endpoint : %s" % self.Endpoint.realm

        self.loop = asyncio.get_event_loop()
        self.coro = loop.create_server(self.factory, self.Endpoint.addr4, self.Endpoint.port)
        self.server = loop.run_until_complete(self.coro)

        print "#) Listening at %s:%d ..." % (self.Endpoint.addr4, self.Endpoint.port)

        try:
            loop.run_forever()
        except KeyboardInterrupt:
            pass
        finally:
            server.close()
            loop.close()

    ############################################################################

    class BasePeer(object):
        def __init__(self, target):
            self._proto = target
            self._hooks = {}

        protocol   = property(lambda self: self._proto)
        factory    = property(lambda self: self.protocol.factory)
        peer       = property(lambda self: self.protocol.peer)

        endpoint   = property(lambda self: self.peer.split(':', 2))

        ip_family  = property(lambda self: self.endpoint[0])
        ip_address = property(lambda self: self.endpoint[1])
        ip_port    = property(lambda self: self.endpoint[2])

        #######################################################################

        def hook(self, ns, rule):
            if ns not in self._hooks:
                self._hooks[ns] = rule

        #***********************************************************************

        def trigger(self, method_or_function, *args, **kwargs):
            hnd = getattr(self, method_or_function, None)

            if callable(hnd):
                return hnd(**raw)

        #***********************************************************************

        def process_bin(self, payload):
            print("Binary message received: {0} bytes".format(len(payload)))

        #***********************************************************************

        def process_txt(self, content):
            print("Text message received: {0}".format(content))

        #***********************************************************************

        def process_doc(self, raw):
            for ns,rule in self._hooks.iteritems():
                try:
                    st = rule(self.ws_peer, raw)
                except:
                    st = False

                if st:
                    try:
                        return self.trigger('on_%s' % ns, **raw)
                    except Exception,ex:
                        print ex

            print raw

        ########################################################################

        def connect(self, request):
            pass

        #***********************************************************************

        def open(self):
            pass

        #***********************************************************************

        def close(self):
            pass

    ############################################################################

    class BaseProtocol(WebSocketServerProtocol):
        @property
        def ws_peer(self):
            if not hasattr(self, '_wrap_peer'):
                hnd = getattr(self, 'Peer', self.BasePeer)

                if callable(hnd):
                    setattr(self, '_wrap_peer', hnd(self))

            return self._wrap_peer

        ########################################################################

        def send_msg(self, raw):
            return self.sendMessage(raw.encode('utf8'), False)

        #***********************************************************************

        def send_json(self, payload):
            return self.send_msg(json.dumps(payload))

        ########################################################################

        def onConnect(self, request):
            print("Client connecting: {0}".format(request.peer))

            self.ws_peer.trigger('connect', request)

        #***********************************************************************

        def onOpen(self):
            print("WebSocket connection open.")

            self.reset()

        #***********************************************************************

        def onMessage(self, payload, isBinary):
            if isBinary:
                self.ws_peer.process_bin(payload)
            else:
                doc = None

                try:
                    doc = json.loads(payload)
                except:
                    pass

                specs = None

                try:
                    specs = yaml.load(payload)
                except:
                    pass

                if doc is not None:
                    #print("JSON document received: {0}".format(repr(doc)))

                    self.ws_peer.process_doc(doc)
                elif specs is not None:
                    #print("YAML specs received: {0}".format(repr(specs)))

                    self.ws_peer.process_doc(doc)
                else:
                    self.ws_peer.process_txt(payload)

        #***********************************************************************

        def onClose(self, wasClean, code, reason):
            print("WebSocket connection closed: {0}".format(reason))

            self.reset()
