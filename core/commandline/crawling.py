#-*- coding: utf-8 -*-

from django2use.shortcuts import *

################################################################################

class API_Walker(BaseCommand):
    # http://daringfireball.net/2009/11/liberal_regex_for_matching_urls
    url_regex = re.compile(r'\b(([\w-]+://?|www[.])[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|/)))')

    #***************************************************************************

    def handle(self, *args, **options):
        self.pool = eventlet.GreenPool()

        for cb,lnk in self.bootstrap():
            self.spwan(cb, lnk)

        print("Started :")

        self.pool.waitall()

        print("Done.")

    #***************************************************************************

    def spawn(self, *args, **kwargs):
        return self.pool.spawn_n(*args, **kwargs)

    ############################################################################

    def fetch(self, url, callback):
        """Fetch a url, stick any found urls into the seen set, and
        dispatch any new ones to the pool."""

        print("fetching", url)

        data = ''

        with eventlet.Timeout(5, False):
            data = urllib2.urlopen(url).read()

            data = json.loads(data)

        feed = data.get('feed', None)

        if feed is not None:
            if callable(callback):
                callback(feed)
