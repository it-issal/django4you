from datetime import datetime

import os ; FILES_DIR = lambda *x: os.path.join(os.path.dirname(os.path.dirname(__file__)), 'files', *x)

#*******************************************************************************

for pth in ('.','logs','static','fixtures'):
    if not os.path.exists(FILES_DIR('logs')):
        os.system('mkdir -p "%s"' % FILES_DIR('logs'))

STARTED_ON = datetime.now()

#*******************************************************************************

USE_I18N = True
USE_L10N = True
USE_TZ   = True

DEBUG    = True
SITE_ID  = 1

#*******************************************************************************

CORE_APPS = (
    'django.contrib.admin',
    'django.contrib.admindocs',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
)

CONTRIB_APPS = (
    'django_extensions',
    'subdomains',
    'corsheaders',
    'djcelery',
    'django_facebook',

    'django2use',
)

SUBDOMAIN_URLCONFS = {
    'apis':      'django2use.urls.APIs',
    'cdn':       'django2use.urls.CDN',
    'www':       'django2use.urls.Website',
}

#*******************************************************************************

MEDIA_URL = '/media/'
STATIC_URL = '/static/'

#*******************************************************************************

MONGO_DBs = {
    'local':     'mongodb://localhost/django2use',
}

DATABASES = {
    'local': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': FILES_DIR('main.sqlite3'),
    },
}

#*******************************************************************************

MEDIA_ROOT = FILES_DIR('media')
STATIC_ROOT = FILES_DIR('static')

STATICFILES_DIRS = (
    FILES_DIR('assets'),
)

FIXTURE_DIRS = (
    FILES_DIR('fixtures'),
)

TEMPLATE_DIRS = (
    FILES_DIR('templates'),
)

#*******************************************************************************

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'access': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': FILES_DIR('logs', 'app-access.log'),
        },
        'error': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': FILES_DIR('logs', 'app-error.log'),
        },
        'traceback': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': FILES_DIR('logs', 'app-debug.log'),
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['access','error'],
            'propagate': True,
        },
    },
}

#*******************************************************************************

MIDDLEWARE_CLASSES = (
    'corsheaders.middleware.CorsMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',

    'django2use.middlewares.core.Augment',
    'django2use.middlewares.vhosting.Router',

    #'subdomains.middleware.SubdomainURLRoutingMiddleware',

    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',

    'django2use.middlewares.core.Profiler',
    'django2use.middlewares.social.Canevas',
)

#*******************************************************************************

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.core.context_processors.request',
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.core.context_processors.tz',
    'django.contrib.messages.context_processors.messages',

    'django_facebook.context_processors.facebook',

    'django2use.middlewares.core.Extend',
    'django2use.middlewares.social.OpenGraph',
)

#*******************************************************************************

TEMPLATE_BUILTINS = (
    'django2use.templatetags.django2use',
    'django2use.templatetags.nestrix',
    'django2use.templatetags.shopping',
)
