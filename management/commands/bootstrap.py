#-*- coding: utf-8 -*-

from django2use.shortcuts import *

################################################################################

class Command(BaseCommand):
    APPs = ('core', 'hub', 'integration', 'crm', 'website', 'jobs', 'retail', 'watch', 'ocean')

    def handle(self, *args, **options):
        #print("Started :")

        self.manage_py('syncdb', '--noinput', '--no-initial-data')

        self.manage_py('makemigrations', *self.APPs)

        self.manage_py('migrate', '--no-initial-data')

        self.manage_py('syncdb', '--noinput')

        #print("Done.")
