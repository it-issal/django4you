#-*- coding: utf-8 -*-

from django2use.shortcuts import *

from django.contrib import admin

from .models import *

################################################################################

class RoleAdmin(admin.ModelAdmin):
    list_display = ('name', 'title')

admin.site.register(Role, RoleAdmin)

################################################################################

class ProfileSSH(admin.TabularInline):
    model = Profile_SSH

class ProfileAdmin(admin.ModelAdmin):
    list_display = ('user', 'email', 'full_name')
    #list_filter  = ('roles__name',)

    inlines = (
        ProfileSSH,
    )

admin.site.register(Profile, ProfileAdmin)

################################################################################

class AuthorAdmin(admin.ModelAdmin):
    list_display  = ('owner','fullname','email')
    list_filter   = ('owner__user',)
    list_editable = ('fullname','email')

admin.site.register(WebAuthor, AuthorAdmin)

#*******************************************************************************

class WidgetInline(admin.TabularInline):
    model = WebWidget

class PageAdmin(admin.ModelAdmin):
    list_display  = ('subdomain','path','created_on','author','title','description')
    list_filter   = ('subdomain','created_on','published_on','last_updated','author')
    list_editable = ('title','description')

    inlines = (
        WidgetInline,
    )

admin.site.register(WebPage, PageAdmin)

#*******************************************************************************

class SettingAdmin(admin.ModelAdmin):
    list_display  = ('language','narrow','litteral')
    list_filter   = ('language','narrow')
    list_editable = ('litteral',)

admin.site.register(WebSetting, SettingAdmin)

#*******************************************************************************

class TranslationAdmin(admin.ModelAdmin):
    list_display  = ('page','language','narrow','litteral')
    list_filter   = ('language','page','narrow')
    list_editable = ('litteral',)

admin.site.register(WebTranslation, TranslationAdmin)

#*******************************************************************************

class SliderInline(admin.TabularInline):
    model = WebSliderEntry

class SliderAdmin(admin.ModelAdmin):
    list_display  = ('page','layout','offset','title','cover')
    list_filter   = ('layout','page')
    list_editable = ('offset','title',)

    inlines = (
        SliderInline,
    )

admin.site.register(WebSlider, SliderAdmin)

#*********************************************************************************************************

#class WidgetAdmin(admin.ModelAdmin):
#    list_display  = ('subdomain','slug','created_on','author','title','description')
#    list_filter   = ('subdomain','slug','created_on','published_on','last_updated','author')
#    list_editable = ('title','description')

#admin.site.register(WebWidget, WidgetAdmin)
