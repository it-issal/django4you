#-*- coding: utf-8 -*-

from django2use.shortcuts import *
from django2use.decorators import *

from django.conf import settings

################################################################################

class Role(models.Model):
    name  = models.CharField(max_length=64)
    title = models.CharField(max_length=256, blank=True)

    def __str__(self):
        return self.title or self.name

################################################################################

class Serializable(models.Model):
    def serialize(self, value, commit=False):
        self.litteral = yaml.dump(value)

        if commit:
            self.save()

        return self.litteral

    def deserialize(self):
        return yaml.load(self.litteral)

    reflection = property(deserialize, serialize)

    def __repr__(self):    return repr(self.reflection)
    def __str__(self):     return str(self.reflection)
    def __unicode__(self): return unicode(self.reflection)

    class Meta:
        abstract = True

################################################################################

class Profile(FacebookModel):
    user         = models.OneToOneField(settings.AUTH_USER_MODEL, related_name='profile')

    #---------------------------------------------------------------------------

    @property
    def __specs__(self):
        try:
            return self.user.__dict__
        except:
            return dict(
                username = 'lambda',
                first_name = 'John',
                last_name  = 'Doe',
                email      = 'john.doe@not-found.org',
            )
    pseudo       = property(lambda self: '%(username)s' % self.__specs__)
    email        = property(lambda self: '%(email)s' % self.__specs__)
    full_name    = property(lambda self: '%(first_name)s %(last_name)s' % self.__specs__)

    avatar_path  = property(lambda self: 'mugshots/%(username)s.jpg' % self.__specs__)

    def get_metadata(self, request):
        try:
            return require_persistent_graph(request).get('me?fields=all')
        except OAuthException,ex:
            return {}
        except Exception,ex:
            raise ex

    #---------------------------------------------------------------------------

    @property
    def total_commits(self):
        resp = []



        return len(resp)

    @property
    def total_ca(self):
        return reduce(operator.add, [prj.total_ca for prj in self.projects.all()] or [0])

    @property
    def total_gain(self):
        return reduce(operator.add, [prj.calculate_gain(self) for prj in self.projects.all()] or [0])

    #---------------------------------------------------------------------------

    def __str__(self):
        return str(self.full_name)

    def __unicode__(self):
        return unicode(self.full_name)

    #---------------------------------------------------------------------------

    @receiver(post_save)
    def create_profile(sender, instance, created, **kwargs):
        """Create a matching profile whenever a user object is created."""
        #if sender == get_user_model():
        #    user = instance
        #    profile_model = get_profile_model()
        #if profile_model == MyCustomProfile and created:
        #    profile, new = MyCustomProfile.objects.get_or_create(user=instance)

    #---------------------------------------------------------------------------

    def get_org_query(self):
        return Q(owner=self) or Q(staff__profile=self) \
            or Q(customers__projects__supervisor=self) \
            or Q(customers__projects__sold_by=self) \
            or Q(customers__projects__developed_by=self) \
            or Q(customers__projects__designed_by=self) \
            or Q(customers__projects__sold_by=self)

    #---------------------------------------------------------------------------

    def get_org_collab(self, org):
        collab = Project.objects.filter(customer__organization=org)

        resp = {
            'gain': reduce(operator.add, [
                x
                for x in [
                    prj.calculate_gain()
                    for prj in collab
                ]
                if x!=0
            ] or [])
        }

        for field,label in [
            ('supervisor',   'projects'),
            ('sold_by',      'sales'),
            ('developed_by', 'applications'),
            ('designed_by',  'designs'),
        ]:
            resp[label] = len(collab.filter({field: self}))

        return resp

    #---------------------------------------------------------------------------

    def get_org_flags(self, org):
        flags = {
            'owner': org.owner.pk==self.pk,
        }

        try:
            staff = org.staff.get(profile)

            flags['founder']      = staff.is_founder
            flags['holder']       = (0 < staff.shares)
        except:
            pass

        return flags

#*******************************************************************************

class Profile_SSH(models.Model):
    profile    = models.ForeignKey(Profile, related_name='ssh_keys')
    username   = models.CharField(max_length=64)

    pub_key    = models.TextField(blank=True)
    priv_key   = models.TextField(blank=True)

################################################################################

class WebAuthor(models.Model):
    owner    = models.ForeignKey(Profile, related_name='authoring')

    fullname = models.CharField(max_length=256, blank=True)
    email    = models.EmailField()

    def __repr__(self):     return "%s <%s>" % (str(self), self.email)
    def __str__(self):      return str(self.fullname or self.owner)
    def __unicode__(self):  return unicode(self.fullname or self.owner)

    #---------------------------------------------------------------------------

    @classmethod
    def default_profile(cls, personna):
        if personna:
            obj, st = WebAuthor.objects.get_or_create(
                owner = personna,
                email = personna.user.email,
            )

            obj.save()

            return obj
        else:
            qs = WebAuthor.objects.all()

            if len(qs):
                return qs[0]
            else:
                return None

#*******************************************************************************

class WebSetting(Serializable):
    language   = models.CharField(max_length=64, null=True, blank=True, default=None)

    narrow     = models.CharField(max_length=512)
    litteral   = models.TextField(blank=True)

################################################################################

class WebPage(models.Model):
    subdomain    = models.CharField(max_length=32)
    path         = models.CharField(max_length=512, blank=True)

    author       = models.ForeignKey(WebAuthor, related_name='publications', blank=True, null=True, default=None)

    #---------------------------------------------------------------------------

    title        = models.CharField(max_length=256, blank=True)
    description  = models.CharField(max_length=256, blank=True)
    keywords     = models.TextField(blank=True)

    content      = models.TextField(blank=True)

    active_slide = models.PositiveIntegerField(default=0)

    #---------------------------------------------------------------------------

    created_on   = models.DateTimeField(auto_now_add=True)
    published_on = models.DateTimeField(blank=True, null=True, default=None)
    last_updated = models.DateTimeField(auto_now_add=True)

    #---------------------------------------------------------------------------

    def __str__(self): return str('%(subdomain)s @ %(path)s' % self.__dict__)
    def __unicode__(self): return unicode('%(subdomain)s @ %(path)s' % self.__dict__)

    #---------------------------------------------------------------------------

    @property
    def total_commits(self):
        resp = []

        return len(resp)

    @property
    def total_ca(self):
        return reduce(operator.add, [prj.total_ca for prj in self.projects.all()] or [0])

    @property
    def total_gain(self):
        return reduce(operator.add, [prj.calculate_gain(self) for prj in self.projects.all()] or [0])

    #---------------------------------------------------------------------------

    def __repr__(self):        return "%(subdomain)s://%(path)s#%(title)s" % self.__dict__
    def __str__(self):         return str(repr(self))
    def __unicode__(self):     return unicode(repr(self))

    #---------------------------------------------------------------------------

    def save(self, *args, **kwargs):
        for k in ['\n','\t','\r']:
            self.keywords = self.keywords.replace(k, ' ')

        self.last_updated = TZ.now()

        return super(WebPage, self).save(*args, **kwargs)

    #---------------------------------------------------------------------------

    @classmethod
    def provision(cls, request, slug=None):
        prf = None

        try:
            prf = request.user.profile
        except:
            prf = None

        obj, st = WebPage.objects.get_or_create(
            author    = WebAuthor.default_profile(prf),
            subdomain = getattr(request, 'subdomain', '') or 'www',
            path      = (request.path[1:] or '/'),
        )

        if obj.path.endswith('.html') or obj.path=='/':
            obj.save()

        return obj

    #---------------------------------------------------------------------------

    class Meta:
        ordering = ['subdomain','path']

#*******************************************************************************

class WebTranslation(Serializable):
    page       = models.ForeignKey(WebPage, related_name='trans')
    language   = models.CharField(max_length=64, null=True, blank=True, default=None)

    narrow     = models.CharField(max_length=512)
    litteral   = models.TextField(blank=True)

    #---------------------------------------------------------------------------

    class Meta:
        ordering = ['page','language','narrow']

#*******************************************************************************

class WebWidget(models.Model):
    page       = models.ForeignKey(WebPage, related_name='widgets')
    provider   = models.CharField(max_length=64)
    config     = models.TextField(default='{}')

#*******************************************************************************

SLIDER_LAYOUTs = (
    ('ltr',     "Image left, Text right"),
    ('rtl',     "Image right, Text left"),
    ('circles', "Circles with texts"),
)

class WebSlider(models.Model):
    page         = models.ForeignKey(WebPage, related_name='sliders')

    layout       = models.CharField(max_length=16, choices=SLIDER_LAYOUTs, default='ltr')
    offset       = models.PositiveIntegerField(default=0)
    css_class    = models.CharField(max_length=256, blank=True)

    title        = models.CharField(max_length=256, blank=True)
    content      = models.TextField(blank=True)

    cover        = models.CharField(max_length=256, blank=True)

    #---------------------------------------------------------------------------

    class Meta:
        ordering = ['page','offset']

#*******************************************************************************

class WebSliderEntry(models.Model):
    slider       = models.ForeignKey(WebSlider, related_name='entries')

    offset       = models.PositiveIntegerField(default=0)
    css_class    = models.CharField(max_length=256, blank=True)
    css_icon     = models.CharField(max_length=64, blank=True)
    content      = models.TextField(blank=True)

    #---------------------------------------------------------------------------

    class Meta:
        ordering = ['slider','offset']
