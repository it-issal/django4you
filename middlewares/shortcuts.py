#-*- coding: utf-8 -*-

import os, sys, logging, re, uuid

from urlparse import urlparse

import operator, git, requests
import simplejson as json

from bson.son import SON

from new import instancemethod

from django.core.handlers.wsgi import WSGIRequest
from django.http               import HttpResponse
from django.utils.cache        import patch_vary_headers

from django.conf import settings
from django.utils.cache import patch_vary_headers

from subdomains.utils import get_domain

logger = logging.getLogger(__name__)
lower = operator.methodcaller('lower')

UNSET = object()
