#-*- coding: utf-8 -*-

from django2use.middlewares.shortcuts import *

################################################################################

def request_redirect(request, target):
    return HttpResponseRedirect(target)

#*******************************************************************************

class Router:
    is_up = False

    def process_request(self, request):
        setattr(request, 'host', request.META["HTTP_HOST"])

        for port in [80, 8000]:
            flg = ':%d' % port

            if request.host[-len(flg):] == flg:
                request.host = request.host[:-len(flg)]

        from django.conf import settings as stt

        if stt.SESSION_COOKIE_DOMAIN in request.host:
            alias = request.host.replace(stt.SESSION_COOKIE_DOMAIN, '')

            if alias in stt.SUBDOMAIN_URLCONFS:
                request.urlconf = stt.SUBDOMAIN_URLCONFS[alias]
            else:
                from django2use.core.routing.mapping import Sitemap

                resp = Sitemap.resolve(alias)

                if resp is not None:
                    request.urlconf = resp.namespace
                else:
                    print "No sitemap found for subdomain : %s" % alias
        else:
            print "No match found for host : %s" % stt.SESSION_COOKIE_DOMAIN

    def process_response(self, request, response):
        from django.conf import settings as stt

        if getattr(stt, 'FORCE_VARY_ON_HOST', True):
            patch_vary_headers(response, ('Host',))

        return response
