#-*- coding: utf-8 -*-

from django2use.middlewares.shortcuts import *

################################################################################

def request_ogp_lookup(request, target):
    return HttpResponseRedirect(target)

def request_ogp_search(request, target):
    return HttpResponseRedirect(target)

class Canevas:
    is_up = False

    def process_request(self, request):
        setattr(request, 'ogp_lookup', instancemethod(request_ogp_lookup, request, WSGIRequest))

        setattr(request, 'ogp_search', instancemethod(request_ogp_search, request, WSGIRequest))

#*******************************************************************************

def OpenGraph(request):
    from django.conf import settings

    try:
        cnt['profile'] = request.user.profile
    except Exception,ex:
        #raise ex

        pass

    return cnt
