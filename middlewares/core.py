#-*- coding: utf-8 -*-

from django2use.middlewares.shortcuts import *

################################################################################

def request_redirect(request, target):
    return HttpResponseRedirect(target)

#*******************************************************************************

def request_render_tpl(request, tpl, **cnt):
    cnt['curr_request'] = request
    cnt['curr_user']    = request.user

    if hasattr(request, 'theme_layout'):
        cnt['curr_layout']  = request.theme_layout

    try:
        cnt['curr_profile'] = request.user.get_profile()
    except:
        cnt['curr_profile'] = None

    return render_to_response(tpl, RequestContext(request, cnt))

#*******************************************************************************

def request_render_json(request, data, **cnt):
    resp = HttpResponse(json.dumps(data))

    resp['Content-Type'] = 'application/json'

    return resp

#*******************************************************************************

class Augment:
    is_up = False

    def process_request(self, request):
        if not self.is_up:
            from django.template.base import add_to_builtins

            #import pdb ; pdb.set_trace()

            from django.conf import settings

            for bltn in settings.TEMPLATE_BUILTINS:
                add_to_builtins(bltn)

            for app in settings.LOCAL_APPS:
                __import__('%s.views' % app)
                #try:
                #    __import__('%s.views' % app)
                #except Exception,ex:
                #    print "Could'nt import application '%s' : %s" % (app, ex)

            self.is_up = True

        setattr(request, 'redirect', instancemethod(request_redirect, request, WSGIRequest))

        setattr(request, 'render_tpl', instancemethod(request_render_tpl, request, WSGIRequest))
        setattr(request, 'render_json', instancemethod(request_render_json, request, WSGIRequest))

        from django2use.models import WebPage

        setattr(request, 'page', WebPage.provision(request))

#*******************************************************************************

class Profiler:
    def process_request(self, request):
        setattr(request, 'profile', None)

        try:
            request.profile = request.profile
        except:
            pass

        if request.profile is None:
            if not request.user.is_anonymous():
                from scubadev.core.models import Profile

                try:
                    request.profile, st = Profile.objects.get_or_create(
                        user = request.user,
                    )
                except Exception,ex:
                    pass
                finally:
                    if request.profile is not None:
                        request.profile.save()

################################################################################

def Extend(request):
    from django.conf import settings

    from django2use.models import WebSetting

    cnt = {
        'CDN_PREFIX':  settings.CDN_PREFIX,
        'page':        request.page,
        'site_config': WebSetting.objects,
    }

    try:
        cnt['profile'] = request.user.profile
    except Exception,ex:
        #raise ex

        pass

    return cnt
