#-*- coding: utf-8 -*-

from django2use.shortcuts import *
from .utils import *

#*******************************************************************************

class MailMessage(mongo.DynamicDocument):
    source        = mongo.DictField(required=True)
    target        = mongo.DictField(required=True)

    title         = mongo.StringField(max_length=512)
    content       = mongo.StringField()

    when          = mongo.DateTimeField(default=TZ.now)
    details       = mongo.DictField()

    def process(self):
        return {
            'status': 'ok',
        }

        hook_signal.send(None, request=request)

        logger.debug('Signal {} sent'.format(hook_signal))
