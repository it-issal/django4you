#-*- coding: utf-8 -*-

from __future__ import with_statement

################################################################################

from datetime import time, date, datetime, timedelta
import os, sys, logging
import re, uuid, operator

#*******************************************************************************

from uuid import uuid4

################################################################################

import requests
from urlparse import urlparse

################################################################################

import simplejson as json, yaml, bson

################################################################################

from bson.son import SON

import pymongo.errors
import mongoengine as mongo
#import mongodbforms as mongo_forms

################################################################################

import eventlet

from eventlet.green import urllib2
