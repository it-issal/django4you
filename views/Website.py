#-*- coding: utf-8 -*-

from django2use.shortcuts import *

from django2use.models import *

from django.contrib import admin, admindocs

################################################################################

fqdn = Sitemap('www', 'django2use.views.Website')

fqdn.enqueue(r'^admin/', include(admin.site.urls))

##############################################################################

PORTFOLIO = [
]

def get_references(request):
    resp = []

    for entry in PORTFOLIO:
        target = dict(entry.iteritems())

        target.update({
            'logo_path': "logos/%(name)s.png" % target,
            'is_active': target['enable'] or request.user.is_superuser,
        })

        resp += [target]

    return resp

################################################################################

def site_render(request, template_path, **cnt):
    cnt.update(dict(
        site = dict(
            title  = "Boukhalef Best Services",
            social = {
                'facebook':    'https://facebook.com/',
                'twitter':     'https://twitter.com/',
                'linkedin':    'https://linkedin.com/',
                'google-plus': 'https://plus.google.com/123456789',
                'skype':       'call:bbs@skype.com',
            },
        ),
        startup = dict(
            title    = "Boukhalef Best Services",
            slang    = "Boukhalef",
            subtitle = "Best Services",

            email    = 'boukhalef.service@gmail.com',
            address  = "296, Complexe Al Irfane, GH 29.\nRue Moulay Rachid.\nBoukhalef.\n9300, Tanger",
            phone    = '5 39 392 810',
            mobiles  = [
                '6 21 808 037',
                '6 00 746 926',
            ],
        ),
        blocks = dict(
            introduction = dict(
                title   = 'Introducing Calypso, <span class="colortext">powerful</span> HTML5 template',
                content = "Highly-professional & modern website template created for you and your business prosperity. <br/> Calypso has all the flexibility and features needed for building a top-notch business website.",
            ),
            featured_work = dict(
                title   = "FEATURED WORK",
                content = "Calypso offers multiple layouts and ways of displaying your content in a manner that best suits for you and <br> your customer. Make beautiful and eye catching site with Calypso today!",
            ),
            timeline = dict(
                title   = "TIMELINE",
                content = "Lorem ipsum dolor sit amet, consectetur adipisicing elit,<br>sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                entries = [
                    dict(
                        when    = '2013-04-10 18:30',
                        icon    = 'lightbulb',
                        title   = "Ricebean black-eyed pea",
                        content = "Winter purslane courgette pumpkin quandong komatsuna fennel green bean cucumber watercress. Pea sprouts wattle seed rutabaga okra yarrow cress avocado grape radish bush tomato ricebean black-eyed pea maize eggplant. Cabbage lentil cucumber chickpea sorrel gram garbanzo plantain lotus root bok choy squash cress potato summer purslane salsify fennel horseradish dulse. Winter purslane garbanzo artichoke broccoli lentil corn okra silver beet celery quandong. Plantain salad beetroot bunya nuts black-eyed pea collard greens radish water spinach gourd chicory prairie turnip avocado sierra leone bologi.",
                    ),
                ],
            ),
        ),
    ))

    try:
        return render(request, template_path, cnt)
    except TemplateDoesNotExist,ex:
        return render(request, 'error/not-found.html', cnt)

################################################################################

#@login_required
@fqdn.route(r'^$', name='site_landing'),
def site_home(request):
    return site_render(request, 'site/landing.html')

################################################################################

#@login_required
@fqdn.route(r'^(?P<path>.+)/$', name='site_page'),
def site_container(request, path):
    cnt = {} # settings.ORG_bbs_STATIC_PAGEs.get(path, {})

    return site_render(request, 'site/static/%s/______.html' % path,

    **cnt)

#@login_required
@fqdn.route(r'^(?P<path>.+)\.html$', name='site_page'),
def site_page(request, path):
    cnt = {} # settings.ORG_bbs_STATIC_PAGEs.get(path, {})

    return site_render(request, 'site/static/%s.html' % path,

    **cnt)

################################################################################

#@login_required
@fqdn.route(r'^contact$', name='site_contact'),
def site_contact(request):
    return site_render(request, 'site/contact.html')

################################################################################

urlpatterns = fqdn.patterns
