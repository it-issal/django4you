#-*- coding: utf-8 -*-

from django2use.shortcuts import *

from django2use.models import *

################################################################################

fqdn = Sitemap.resolve('apis')

################################################################################

@fqdn.route(r'^public/datetime.json$', name='public_datetime', csrf=False, policy='anon'),
def date_time(request):
    resp = {}

    return request.render_json(resp)

################################################################################

@fqdn.route(r'^public/geo.json$', name='public_datetime', csrf=False, policy='anon'),
def geo_system(request):
    resp = {}

    return request.render_json(resp)

################################################################################

@fqdn.route(r'^public/prayer.json$', name='public_datetime', csrf=False, policy='anon'),
def prayer(request):
    resp = {}

    return request.render_json(resp)

################################################################################

@fqdn.route(r'^public/weather.json$', name='public_datetime', csrf=False, policy='anon'),
def weather(request):
    resp = requests.get('http://api.openweathermap.org/data/2.5/weather', params={
        'q': request.GET.get('q', 'Tangier,MA'),
    })

    return request.render_json(resp.json())

################################################################################

@fqdn.route(r'^public/currency.json$', name='public_datetime', csrf=False, policy='anon'),
def devise_conv(request):
    resp = {}

    return request.render_json(resp)
