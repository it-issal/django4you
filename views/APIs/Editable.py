#-*- coding: utf-8 -*-

from django2use.shortcuts import *

from django2use.models import *

################################################################################

fqdn = Sitemap.resolve('apis')

################################################################################

@csrf_exempt
@fqdn.route(r'^editable/trans/dynamic$', name='editable_trans_dynamic', csrf=False, policy='anon'),
def trans_dynamic(request):
    resp = request.POST.get('value')

    ts = None

    if 'page' in request.GET:
        pg = None

        try:
            pg = WebPage.objects.get(pk=int(request.GET.get('page')))
        except Exception,ex:
            print ex
            print request.GET

            return HttpResponse(unicode(resp))

        ts, st = WebTranslation.objects.get_or_create(
            page     = pg,
            narrow   = request.GET.get('narrow'),
            language = request.GET.get('lang', 'en'),
        )
    else:
        ts, st = WebSetting.objects.get_or_create(
            narrow   = request.GET.get('narrow'),
            language = request.GET.get('lang', 'en'),
        )

    if ts is not None:
        ts.serialize(resp)

        ts.save()

        return HttpResponse(unicode(ts))
    else:
        return HttpResponse(unicode(resp))

################################################################################

@fqdn.route(r'^editable/orm/(?P<sch>.+)/(?P<nrw>[\w\d\-\_\.\,\;]+)/(?P<field>.+)$', name='editable_model_field'),
def model_field(request, sch, nrw, field):
    resp = request.POST.get('value')

    from django.db.models.loading import get_model

    mdl = get_model(sch)

    obj = mdl.objects.get(
        pk__in=[
            nrw,
            nrw.lower(),
            nrw.capitalize(),
            nrw.upper(),
            nrw.strip(),
            int(nrw),
        ],
    )

    if obj is not None:
        setattr(obj, field, resp)

        obj.save()

    return HttpResponse(unicode(resp))
