#-*- coding: utf-8 -*-

from django2use.shortcuts import *

from django2use.models import *

################################################################################

fqdn = Sitemap('apis', 'django2use.views.APIs')

##############################################################################

from . import Editable
from . import Public

################################################################################

urlpatterns = fqdn.patterns
