#-*- coding: utf-8 -*-

from django2use.shortcuts import *

from django2use.models import *

################################################################################

fqdn = Sitemap('cdn', 'django2use.views.APIs')

##############################################################################

from . import Editable
from . import Public

################################################################################

from django.conf.urls.static import static

urlpatterns  = fqdn.patterns
urlpatterns += static(settings.MEDIA_URL,  document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
